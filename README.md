## Basic Android MVP - PipeDrive test assignment

* I answered point number one and two by actually building and desinging the  with the 
* technologies that can potentially cover the criterias mentioned. 

* A lot of effort and learning was put into this project [PROJECT Commits](https://bitbucket.org/aliengamedev/pipelightrx/commits/branch/master)

* [You can find me on FB in my android PAGE](https://www.facebook.com/LienAndroid) Here i paste minimal ideas while working with android stuff and of course i make my concerncs and mistakes public.  

You can get the APK [FROM HERE](https://bitbucket.org/aliengamedev/pipelightrx/raw/848f4ba2236e800ae7befba9ec0475fead00c240/app/app-release.apk)

For the frist point the effort went into build a scalable infraestructure in which i could develop bringing to the table recent patterns and 
libraries. 

* The current solutions is an example  of Model View Presenter  + Basic eactive Programming + DB persistance using DBFLow 

#2. Describe how would you desing an app and what technologies would you use. 

## In general i would use MVP for separation of concerns and organization of the app, i mention more about this in the wiki [PROJECT WIKI](https://bitbucket.org/aliengamedev/pipelightrx/wiki/Home)

## JavaRx as the backbone for handling asych events in the app by composing async events  with observable sequences. 

* 
a. To support background updates of data i would chosse JavaRx to bind the diffrent components of the  application
    in a non blocking way avoding making call in the UI thread and relying on cache and persistence  while the network service is restored. 
* 
b . Operate with 10 to 100 of objects i would try  a PagedList that can load data in chunks and allows further load 
    operations with the PagedListAdapter his class is a convenience wrapper around PagedListAdapterHelper that implements
	common default behavior for item counting, and listening to PagedList update callbacks which can do great on keeping 
	the recycler view out of trouble. Also doing proper caching by using a mix strategy the in memory and persistance.
    one nice place too look at is here [Paging Library ](https://developer.android.com/topic/libraries/architecture/paging.html)
    when this lib  is used combined with Room  it can also be  using with a TiledDataSource that can generate pages at 
    provided fix size. 
* 
c. For this point for keeping the UI in sync with the Storage changes i would create a HOT Observable and compose an 
   a stream that would emit changes to the upper layers of the applicacion in a thread safe maner making the app react 
   to these events. 
   

* 
d . Creating base clases that override the all the methos involved in the Activity of Fragment lifecycle change  like 
    onStart, onDestroy, onStop,  using SharaedPreferances to share minimal information that can help me recover the activity
	to it's previous state and also when it's possible use so other caching techniques using persistence.