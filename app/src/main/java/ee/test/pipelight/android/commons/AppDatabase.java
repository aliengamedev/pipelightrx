package ee.test.pipelight.android.commons;

import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;

import ee.test.pipelight.android.login.model.local.db.UsserSession;
import timber.log.Timber;

/**
 * Created by John on 10/22/2017.
 */
@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public class AppDatabase {
    public static final String NAME = "AppDatabase";
    public static final int VERSION = 8;

    @Migration(version = 7, database = AppDatabase.class)
    public static class Migration7 extends AlterTableMigration<UsserSession> {

        public Migration7(Class<UsserSession> table) {
            super(table);
            Timber.d("Trying out migration");
        }

        @Override
        public void onPreMigrate() {
            addColumn(SQLiteType.TEXT, "addtionalData");
        }
    }
}
