package ee.test.pipelight.android.persons.model.remote;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import ee.test.pipelight.android.commons.base.BaseInteractor;
import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.persons.PersonsContract;
import ee.test.pipelight.android.persons.model.PersonsDataSource;
import ee.test.pipelight.android.persons.model.remote.dto.Datum;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by John on 10/23/2017.
 */

public class PersonsRemoteDataSource extends BaseInteractor<PersonsResponse> implements PersonsDataSource {

    @Nullable
    private static PersonsRemoteDataSource INSTANCE;
    private PersonsContract.Model listener;

    private PersonsRemoteDataSource(@NonNull PersonsContract.Model listener) {
        this.listener = listener;
    }

    private static final LinkedHashMap<String, Datum> LOGIN_PERSONS_DATA;

    static {
        LOGIN_PERSONS_DATA = new LinkedHashMap<>(2);

    }

    public static PersonsRemoteDataSource getInstance(@NonNull PersonsContract.Model listener) {
        if (INSTANCE == null) {
            INSTANCE = new PersonsRemoteDataSource(listener);
        }
        return INSTANCE;
    }

    @Override
    public Observable<Response<PersonsResponse>> getPersons() {
        Retrofit retrofit = initRetrofit();
        PersonsService personsService = retrofit.create(PersonsService.class);

        if (LOGIN_PERSONS_DATA.size() == 0) {
            //TODO:Cache is clean -> check DB ->[sessionFound -> returns UserSession ,sessionNotFound->promnts to Login]
            return personsService.persons(BaseInteractor.API_TOKEN);
        } else {
            Observable<Response<PersonsResponse>> emptyResponse = getResponseObservable();
            return emptyResponse;
        }
    }

    @Override
    public void savePersons(@NonNull Datum userSession, @NonNull AdditionalData additionalData) {

    }

    private Observable<Response<PersonsResponse>> getResponseObservable() {
        final List<Datum> datums = new ArrayList<>();
        Observable.fromIterable(LOGIN_PERSONS_DATA.values()).subscribe(new Consumer<Datum>() {
            @Override
            public void accept(@io.reactivex.annotations.NonNull Datum datum) throws Exception {
                datums.add(datum);
            }
        }).dispose();
        AdditionalData additionalData1 = new AdditionalData();
        Response<PersonsResponse> loginResponseResponse = Response.success(new PersonsResponse(datums, additionalData1));
        return Observable.just(loginResponseResponse);
    }
}
