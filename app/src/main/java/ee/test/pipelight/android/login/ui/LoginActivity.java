package ee.test.pipelight.android.login.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import ee.test.pipelight.BuildConfig;
import ee.test.pipelight.R;
import ee.test.pipelight.R2;
import ee.test.pipelight.android.commons.base.BaseActivity;
import ee.test.pipelight.android.login.LoginContract;
import ee.test.pipelight.android.login.LoginListPresenter;
import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.login.model.remote.dto.Company;
import ee.test.pipelight.android.login.model.remote.dto.Info;
import ee.test.pipelight.android.login.model.remote.dto.Profile;
import ee.test.pipelight.android.login.model.remote.dto.UserSession;
import ee.test.pipelight.android.persons.ui.PersonListActivity;
import timber.log.Timber;


public class LoginActivity extends BaseActivity implements LoginContract.View {

    @NonNull
    @BindView(R2.id.login_pipedrive_username)
    EditText pipedriveEmailEditText;
    @NonNull
    @BindView(R2.id.login_pipedrive_password)
    EditText fakePasswordEditText;
    @BindView(R2.id.login_check_remember)
    CheckBox rememberMe;

    LoginListPresenter presenter;

    @Nullable
    UserSession userSession;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupToolbar(getString(R.string.app_name));

        setPresenter(presenter = new LoginListPresenter(this));
        presenter.getCachedUserSession();
        if (BuildConfig.DEBUG) {
            ButterKnife.setDebug(true);
        }

    }

    @OnClick(R2.id.login_submit_button)
    public void loginTapped(View view) {
        String email = pipedriveEmailEditText.getText().toString();
        String password = fakePasswordEditText.getText().toString();
        // Pass user event straight to presenter
        presenter.authenticateUser(email, password, rememberMe.isChecked());
    }

    @Override
    public void showLoadingIndicator() {
        messageDialog("Loading", "Processing", R.mipmap.ic_icon_android_processing);
    }

    @Override
    public void showError(Throwable throwable) {
        messageDialog("AppError", throwable.getLocalizedMessage(), R.mipmap.ic_icon_android_fatal);
        //TODO:change Logger
        // Logger.e(throwable.getLocalizedMessage());
    }

    @Override
    public void showLoginError(String msgError) {
        messageDialog("Login Error", msgError, R.mipmap.ic_icon_android_error);
    }

    @Override
    public void navigateToPersonListActivity() {
        messageDialog("Login Result", "Successful", false, true, R.mipmap.ic_icon_android_ok);
        Intent intent = new Intent(this, PersonListActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoadCacheUserSession(UserSession userSession, AdditionalData additionalData) {
        this.userSession = userSession;
        Company company = this.userSession.getCompany();
        Info info = company.getInfo();
        String name = info.getName();
        Profile profile = additionalData.getUser().getProfile();
        pipedriveEmailEditText.setText(profile.getEmail());
        fakePasswordEditText.setText("CosmosPipe11");
        messageDialog("Welcomeback", name, false, true, R.mipmap.ic_icon_android_ok);
    }

    @OnTouch(R2.id.login_pipedrive_container_scroll_view)
    public boolean onTouchScrollView(View scroll, MotionEvent event) {
        //TODO:change Logger
        //Logger.d("SCroll touched");
        hideKeyboard(scroll);
        return true;
    }

    @OnClick(R2.id.login_gplus)
    public void gplusTapped(View view) {
        messageDialog("Oopps", "This is just a demo, maybe we can do business?. \n By the way i'm forgeting the session", false, true, R.mipmap.ic_icon_android_fatal);
        presenter.doForgetSession();
    }

    @OnClick(R2.id.login_check_remember)
    public void rememberMe(View view) {
        //messageDialog("Oopps", "This is just a demo, maybe we can do business?. \n By the way i'm forgeting the session", false, true, R.mipmap.ic_icon_android_fatal);
        Timber.d("rememberME:" + rememberMe.isChecked());
        presenter.doRememberSession(rememberMe.isChecked());
    }

}
