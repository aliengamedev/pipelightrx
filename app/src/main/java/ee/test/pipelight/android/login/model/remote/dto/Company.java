package ee.test.pipelight.android.login.model.remote.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by John on 10/18/2017.
 */

public class Company {

    @SerializedName("info")
    private Info info;
    @SerializedName("features")
    private List<String> features = null;
    @SerializedName("settings")
    private Settings settings;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<String> getFeatures() {
        return features;
    }

    public void setFeatures(List<String> features) {
        this.features = features;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }
}
