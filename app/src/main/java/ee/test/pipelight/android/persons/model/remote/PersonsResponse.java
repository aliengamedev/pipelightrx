package ee.test.pipelight.android.persons.model.remote;

import java.util.List;

import ee.test.pipelight.android.commons.base.BaseResponse;
import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.persons.model.remote.dto.Datum;

/**
 * Created by John on 10/23/2017.
 */

public class PersonsResponse extends BaseResponse<List<Datum>, AdditionalData> {

    public PersonsResponse(List<Datum> userSessions, AdditionalData additionalData) {
        super(userSessions, additionalData);
    }
}
