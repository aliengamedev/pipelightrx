package ee.test.pipelight.android.persons;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.util.List;

import ee.test.pipelight.android.persons.model.PersonRepository;
import ee.test.pipelight.android.persons.model.local.PersonsLocalDataSource;
import ee.test.pipelight.android.persons.model.remote.PersonsRemoteDataSource;
import ee.test.pipelight.android.persons.model.remote.PersonsResponse;
import ee.test.pipelight.android.persons.model.remote.dto.Datum;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by John on 10/22/2017.
 */

public class PersonsPresenter implements PersonsContract.Presenter, PersonsContract.Model<PersonsResponse> {

    private PersonsContract.View view;

    private final PersonRepository personRepository;
    @NonNull
    private CompositeDisposable compositeDisposable;


    public PersonsPresenter(PersonsContract.View view) {
        this.view = view;
        this.personRepository = PersonRepository.getInstance(this, PersonsRemoteDataSource.getInstance(this), PersonsLocalDataSource.getInstance(this));
    }

    @Override
    public void onNetworkFailure(Throwable throwable) {
        view.showError(throwable);
    }

    @Override
    public void attachView(PersonsContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void subscribe() {
        loadCompositeDisposable();
    }

    @Override
    public void unsubscribe() {
        if (compositeDisposable != null) {
            if (!compositeDisposable.isDisposed()) {
                compositeDisposable.clear();
            }
        }

    }

    @Override
    public void destroy() {
        dispose();
    }

    @Override
    public void result(int requestCode, int resultCode) {

    }

    //----
    @Override
    public void getCachedPersons() {

    }

    @Override
    public void getPersons() {

        view.showLoadingIndicator();
        loadCompositeDisposable();
        compositeDisposable.add(personRepository.getPersons()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .cache()
                .subscribe(new Consumer<Response<PersonsResponse>>() {
                    @Override
                    public void accept(Response<PersonsResponse> userResponse) throws Exception {
                        handleResponse(userResponse);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        handleError(throwable);
                    }
                }));

    }
    //----

    @Override
    public void onLoadCachedPersons(PersonsResponse loadedPersons) {
        view.onLoadedCachePersons(loadedPersons);
    }

    @Override
    public void onNetworkRequestSuccess(PersonsResponse persons, Response response) {
        view.onLoadedPersons(persons);

    }

    @Override
    public void onNetworkRequestFailed(PersonsResponse persons, Response response) {

    }


    @Override
    public void handleResponse(Response<PersonsResponse> response) {
        Timber.d(response.body().getData().toString());
        Timber.d("Code: " + response.code() + " Message: " + response.message());
        PersonsResponse body = response.body();

        if (body == null) {
            handleBodyErrorResponse(response);
        } else {
            handleBodyResponse(response);
            List<Datum> datums = body.getData();
            for (Datum datum : datums) {
                personRepository.savePersons(datum,body.getAdditionalData());
            }

        }


    }

    @Override
    public void handleBodyResponse(Response<PersonsResponse> response) {
        Timber.d(response.body().toString());
        onNetworkRequestSuccess(response.body(), response);

    }

    @Override
    public void handleBodyErrorResponse(Response<PersonsResponse> responseError) {
        Gson gson = new Gson();
        PersonsResponse personsResponse = gson.fromJson(responseError.errorBody().charStream(), PersonsResponse.class);
        onNetworkRequestFailed(personsResponse, responseError);

    }

    @Override
    public void handleError(Throwable throwable) {
        Timber.d(throwable.toString());
    }

    private void loadCompositeDisposable() {
        if (compositeDisposable != null) {
            if (compositeDisposable.isDisposed()) {
                compositeDisposable = new CompositeDisposable();
            }
            return;
        }
        compositeDisposable = new CompositeDisposable();
    }

    private void dispose() {
        compositeDisposable.dispose();
    }
}
