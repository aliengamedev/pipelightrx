package ee.test.pipelight.android.login.model.remote;

import ee.test.pipelight.android.login.model.remote.dto.BaseDto;

/**
 * Created by John on 10/18/2017.
 */

public class LoginRequest extends BaseDto {
    private static final long serialVersionUID = -7221900186797326541L;
    private String name;
    private String email;
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public String toString() {
        return "LoginRequest{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
