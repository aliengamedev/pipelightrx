package ee.test.pipelight.android.login.model.remote.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/18/2017.
 */

public class UserSession {

    @SerializedName("user_id")
    private Integer userId;
    @SerializedName("company_id")
    private Integer companyId;
    @SerializedName("api_token")
    private String apiToken;
    @SerializedName("add_time")
    private String addTime;
    @SerializedName("company")
    private Company company;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "UserSession{" +
                "userId=" + userId +
                ", companyId=" + companyId +
                ", apiToken='" + apiToken + '\'' +
                ", addTime='" + addTime + '\'' +
                ", company=" + company +
                '}';
    }

}
