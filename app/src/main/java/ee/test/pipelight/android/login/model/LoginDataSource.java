package ee.test.pipelight.android.login.model;

import android.support.annotation.NonNull;

import java.util.List;

import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.login.model.remote.LoginResponse;
import ee.test.pipelight.android.login.model.remote.dto.UserSession;
import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by John on 10/20/2017.
 */

public interface LoginDataSource {

    Observable<Response<LoginResponse>> getUserSession(@NonNull String username, @NonNull String password,boolean rememberUser);

    Observable<List<UserSession>> getUserSession();

    Observable<Response<LoginResponse>> getCacheUserSession();

    void saveUserSession(@NonNull UserSession userSession,@NonNull AdditionalData additionalData);

    void deleteUserSession(@NonNull String usrSessionId);

    void rememberUserSession(boolean remember);
}
