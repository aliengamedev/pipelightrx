package ee.test.pipelight.android.login.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import java.util.List;

import ee.test.pipelight.android.login.LoginContract;
import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.login.model.remote.LoginResponse;
import ee.test.pipelight.android.login.model.remote.dto.UserSession;
import io.reactivex.Observable;
import io.reactivex.functions.Predicate;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by John on 10/21/2017.
 */

public class LoginRepository implements LoginDataSource {


    @NonNull
    private final LoginDataSource remoteDataSource;

    @NonNull
    private final LoginDataSource localDataSource;

    @Nullable
    private static LoginRepository INSTANCE;

    private boolean persistSession;

    private LoginRepository(@NonNull LoginContract.Model listener, @NonNull LoginDataSource remoteDataSource, @NonNull LoginDataSource localDataSource) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
    }


    public static LoginRepository getInstance(@NonNull LoginContract.Model listener, @NonNull LoginDataSource remoteDataSource, @NonNull LoginDataSource localDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new LoginRepository(listener, remoteDataSource, localDataSource);
        }
        return INSTANCE;
    }


    @Override
    public Observable<Response<LoginResponse>> getUserSession(@NonNull String username, @NonNull String password, @NonNull boolean rememberUser) {
        this.persistSession = rememberUser;
        return Observable.concat(remoteDataSource.getUserSession(username, password, rememberUser)
                ,localDataSource.getUserSession(username,password,rememberUser))
                .filter(new Predicate<Response<LoginResponse>>() {
                    @Override
                    public boolean test(@io.reactivex.annotations.NonNull Response<LoginResponse> loginResponseResponse) throws Exception {
                        return loginResponseResponse==null?false:true;
                    }
                });
    }


    @Override
    public Observable<Response<LoginResponse>> getCacheUserSession() {
        return localDataSource.getCacheUserSession();
    }

    @Override
    public Observable<List<UserSession>> getUserSession() {
        return remoteDataSource.getUserSession();
    }

    @Override
    public void saveUserSession(@NonNull UserSession userSession,@NonNull AdditionalData additionalData) {
        Gson gson = new Gson();
        Timber.d("userSession: " + gson.toJson(userSession));
        remoteDataSource.saveUserSession(userSession,additionalData);
        if (persistSession) {
            Timber.d("userSession Persist in DB");
            localDataSource.saveUserSession(userSession,additionalData);
        }
    }

    @Override
    public void deleteUserSession(@NonNull String usrSessionId) {
        remoteDataSource.deleteUserSession(usrSessionId);
        localDataSource.deleteUserSession(usrSessionId);
    }

    @Override
    public void rememberUserSession(boolean remember) {
        remoteDataSource.rememberUserSession(remember);
        localDataSource.rememberUserSession(remember);
    }
}
