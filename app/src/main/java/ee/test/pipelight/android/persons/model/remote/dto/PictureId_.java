package ee.test.pipelight.android.persons.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/23/2017.
 */

public class PictureId_ {

    @SerializedName("item_type")
    @Expose
    public String itemType;
    @SerializedName("item_id")
    @Expose
    public Integer itemId;
    @SerializedName("active_flag")
    @Expose
    public Boolean activeFlag;
    @SerializedName("add_time")
    @Expose
    public String addTime;
    @SerializedName("update_time")
    @Expose
    public String updateTime;
    @SerializedName("added_by_user_id")
    @Expose
    public Integer addedByUserId;
    @SerializedName("pictures")
    @Expose
    public Object pictures;
    @SerializedName("value")
    @Expose
    public Integer value;

    @Override
    public String toString() {
        return "PictureId_{" +
                "itemType='" + itemType + '\'' +
                ", itemId=" + itemId +
                ", activeFlag=" + activeFlag +
                ", addTime='" + addTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", addedByUserId=" + addedByUserId +
                ", pictures=" + pictures +
                ", value=" + value +
                '}';
    }
}
