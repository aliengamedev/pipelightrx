package ee.test.pipelight.android.login.model.local;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.OperatorGroup;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import ee.test.pipelight.android.commons.AppDatabase;
import ee.test.pipelight.android.login.LoginContract;
import ee.test.pipelight.android.login.model.LoginDataSource;
import ee.test.pipelight.android.login.model.local.db.UsserSession;
import ee.test.pipelight.android.login.model.local.db.UsserSession_Table;
import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.login.model.remote.LoginResponse;
import ee.test.pipelight.android.login.model.remote.dto.UserSession;
import io.reactivex.Observable;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by John on 10/20/2017.
 */

public class LoginLocalDataSource implements LoginDataSource {

    @Nullable
    private static LoginLocalDataSource INSTANCE;
    //Todo:Probably i don't need this here Login Data source should remain separated
    private LoginContract.Model listener;
    private DatabaseDefinition db;
    private Gson gson;

    private boolean rememberSession;

    private LoginLocalDataSource(@NonNull LoginContract.Model listener) {
        this.listener = listener;
        db = FlowManager.getDatabase(AppDatabase.class);
        gson = new Gson();
        Timber.d("DB: started " + db.getDatabaseFileName());
    }

    public static LoginLocalDataSource getInstance(
            @NonNull LoginContract.Model listener) {
        if (INSTANCE == null) {
            INSTANCE = new LoginLocalDataSource(listener);
        }
        return INSTANCE;
    }

    @Override
    public Observable<Response<LoginResponse>> getUserSession(@NonNull String username, @NonNull String password, boolean remembercheckSession) {
        return getResponseObservable();
    }

    /**
     *
     * @return returns empty reposnse or DB saves session
     */
    private Observable<Response<LoginResponse>> getResponseObservable() {
        List<UsserSession> storedUserssesions = new Select()
                .from(UsserSession.class)
                .queryList();
        if (storedUserssesions.isEmpty()) {
            //Empty list response example
            //Response<LoginResponse> loginResponseResponse = Response.success(new LoginResponse());
            //Optional.absent() = Response.success(new LoginResponse(userSessions));
            Observable<Response<LoginResponse>> obLoginEmptyResponseResponse = Observable.empty();
            return obLoginEmptyResponseResponse;
        }else{
            AdditionalData additionalData=new AdditionalData();
            List<UserSession> userSessions = new ArrayList<>();
            for (UsserSession dbSession : storedUserssesions) {
                additionalData=gson.fromJson(dbSession.getAdditionalData(), AdditionalData.class);
                userSessions.add(gson.fromJson(dbSession.getJson(), UserSession.class));
            }
            Response<LoginResponse> loginResponseResponse = Response.success(new LoginResponse(userSessions,additionalData));
            return Observable.just(loginResponseResponse);
        }
    }

    @Override
    public Observable<List<UserSession>> getUserSession() {
        return null;
    }

    @Override
    public Observable<Response<LoginResponse>> getCacheUserSession() {
        return getResponseObservable();
    }

    @Override
    public void saveUserSession(@NonNull UserSession userSession,@NonNull AdditionalData additionalData) {
        boolean isSaved = false;

        if (rememberSession) {
            Timber.d("DB: usserSessionDb");
            UsserSession usserSessionDb = new UsserSession();
            OperatorGroup orGroup = OperatorGroup.clause();
            orGroup.or(UsserSession_Table.timestamp.is(userSession.getAddTime()));
            List<UsserSession> storedUserssesions = new Select()
                    .from(UsserSession.class)
                    .where(orGroup)
                    .queryList();

            if (storedUserssesions.isEmpty()) {
                usserSessionDb.setApiToken(userSession.getApiToken());
                usserSessionDb.setJson(gson.toJson(userSession));
                usserSessionDb.setTimestamp(userSession.getAddTime());
                usserSessionDb.setAdditionalData(gson.toJson(additionalData));
                isSaved = usserSessionDb.save();
                Timber.d("DB: usserSessionDb  isSaved= " + isSaved);
            } else {
                Timber.d("DB: usserSessionDb  isSaved= " + isSaved + " userSession.timeStamp exist whith" + userSession.getAddTime());
            }
        } else {
            Timber.d("DB: usserSessionDb  user Choose not to be remembered");
        }

    }

    @Override
    public void deleteUserSession(@NonNull String usrSessionId) {
        Delete.table(ee.test.pipelight.android.login.model.local.db.UsserSession.class);
    }

    @Override
    public void rememberUserSession(boolean remember) {
        this.rememberSession = remember;
    }

}
