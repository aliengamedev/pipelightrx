package ee.test.pipelight.android.login.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/19/2017.
 */

public class PersonBlockOrder {
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("visible")
    @Expose
    public Boolean visible;
}
