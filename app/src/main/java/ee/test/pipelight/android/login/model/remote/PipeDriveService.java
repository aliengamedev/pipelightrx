package ee.test.pipelight.android.login.model.remote;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by jpotts18 on 5/12/15.
 */

public interface PipeDriveService {
    @Headers({"Accept: application/json"})
    @POST("authorizations")
    Observable<Response<LoginResponse>> login(@Body LoginRequest loginRequest);
}