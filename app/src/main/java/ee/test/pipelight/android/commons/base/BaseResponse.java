package ee.test.pipelight.android.commons.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public abstract class BaseResponse<T,P>  {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("error_info")
    @Expose
    private String errorInfo;
    @SerializedName("data")
    @Expose
    private T data;
    @SerializedName("additional_data")
    @Expose
    private P additionalData;

    public BaseResponse(T userSessions,P additionalData) {
        this.data=userSessions; this.additionalData=additionalData;
       }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "success=" + success +
                ", error='" + error + '\'' +
                ", errorInfo='" + errorInfo + '\'' +
                ", data=" + data +
                ", additionalData=" + additionalData +
                '}';
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(String errorInfo) {
        this.errorInfo = errorInfo;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public P getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(P additionalData) {
        this.additionalData = additionalData;
    }
}
