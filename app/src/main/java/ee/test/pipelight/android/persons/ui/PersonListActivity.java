package ee.test.pipelight.android.persons.ui;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ee.test.pipelight.BuildConfig;
import ee.test.pipelight.R;
import ee.test.pipelight.R2;
import ee.test.pipelight.android.commons.base.BaseActivity;
import ee.test.pipelight.android.login.model.remote.dto.UserSession;
import ee.test.pipelight.android.persons.PersonsContract;
import ee.test.pipelight.android.persons.PersonsPresenter;
import ee.test.pipelight.android.persons.model.remote.PersonsResponse;
import ee.test.pipelight.android.persons.model.remote.dto.Datum;
import ee.test.pipelight.android.persons.model.remote.dto.Person;
import timber.log.Timber;


public class PersonListActivity extends BaseActivity implements PersonsContract.View {

    PersonsPresenter presenter;
    @Nullable
    UserSession userSession;
    @NonNull
    @BindView(R2.id.persons_recycler_view)
    RecyclerView personsRecyclerView;
    public static final int REQUEST_CALL_PERMISSION = 1;
    public static final int REQUEST_CALLING = 2;
    PersonListAdapter personListAdapter;
    PersonsResponse persons;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_persons_recycler_view);
        setupToolbar(getString(R.string.app_name));
        setPresenter(presenter = new PersonsPresenter(this));
        if (BuildConfig.DEBUG) {
            ButterKnife.setDebug(true);
        }

     /*  List<Person> persons = new ArrayList<>();
        Person person1 = new Person();
        person1.setName("Joe");
        person1.setPhone("555555555");

        Person person2 = new Person();
        person2.setName("Dan");
        person2.setPhone("777777777");

        persons.add(person1);
        persons.add(person2);*/

      /*  for(int i=0;i<100;i++){
            Person p =new Person();
            p.setName("Jhon"+i);
            p.setPhone(new Date().getTime()+"");
            datums.add(p);
        }*/

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        personsRecyclerView.setLayoutManager(layoutManager);
        personsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        personListAdapter = new PersonListAdapter(this, new ArrayList<Datum>());
        personsRecyclerView.setAdapter(personListAdapter);
        presenter.getPersons();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CALL_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    //onPermissionsGranted();
                    //Todo:Make a call for example with icons of the PersonList
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == REQUEST_CALLING) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Timber.i("RESULT_OK");
            }
        }
    }


    @Override
    public void showLoadingIndicator() {

    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void showTokenError(String msgError) {

    }

    @Override
    public void navigateToPersonDetailsActivity() {

    }

    @Override
    public void onLoadedCachePersons(PersonsResponse cachedPersons) {

    }

    @Override
    public void onLoadedPersons(PersonsResponse persons) {
        List<Datum> data = persons.getData();
        personListAdapter.updateAdapter(data);
    }

}
