package ee.test.pipelight.android.login;

import ee.test.pipelight.android.commons.base.BasePresenter;
import ee.test.pipelight.android.commons.base.BaseView;
import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.login.model.remote.LoginResponse;
import ee.test.pipelight.android.login.model.remote.dto.UserSession;
import retrofit2.Response;

/**
 * Created by John on 10/16/2017.
 */
public interface LoginContract {

   interface View extends BaseView<LoginListPresenter> {
        //Presenter -> View
        void showLoadingIndicator();
        void showError(Throwable throwable);
        void showLoginError(String msgError);
        void navigateToPersonListActivity();
        void onLoadCacheUserSession(UserSession userSession,AdditionalData additionalData);
      }

    interface Presenter extends BasePresenter<View> {
        //Presenter-> Model
        void authenticateUser(String user, String password,boolean rememberUser);
        //Model -> Presenter
        void result(int requestCode, int resultCode);
        //View -> Presenter
        void doForgetSession();
        //View -> Presenter
        void doRememberSession(boolean rememberUser);
        //View -> Presenter
        void getCachedUserSession();
        //Presenter -> View
        void onLoadCachedUserSession(Response<LoginResponse> response);
    }

    interface Model<T> {
        // Presenter ->View
        void onNetworkRequestSuccess(LoginResponse userSession, Response response);
        void onNetworkRequestFailed(LoginResponse userSession, Response response);
        void onNetworkFailure(Throwable throwable);
        //Model -> Presenter
        void handleResponse(Response<T> response);
        void handleBodyResponse(Response<T> response);
        void handleBodyErrorResponse(Response<T> responseError);
        void handleError(Throwable throwable);
        //Presenter -> Model


    }
}