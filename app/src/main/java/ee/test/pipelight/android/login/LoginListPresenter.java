package ee.test.pipelight.android.login;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.util.List;

import ee.test.pipelight.android.commons.base.BaseInteractor;
import ee.test.pipelight.android.login.model.LoginRepository;
import ee.test.pipelight.android.login.model.local.LoginLocalDataSource;
import ee.test.pipelight.android.login.model.remote.LoginRemoteDataSource;
import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.login.model.remote.LoginResponse;
import ee.test.pipelight.android.login.model.remote.dto.UserSession;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by  jhon on 10/21/17.
 */
public class LoginListPresenter implements LoginContract.Presenter, LoginContract.Model<LoginResponse> {

    private LoginContract.View view;
    private final LoginRepository loginRepository;
    @NonNull
    private CompositeDisposable compositeDisposable;

    public LoginListPresenter(LoginContract.View view) {
        this.view = view;
        loginRepository = LoginRepository.getInstance(this, LoginRemoteDataSource.getInstance(this), LoginLocalDataSource.getInstance(this));
    }

    @Override
    public void onNetworkRequestSuccess(LoginResponse userSession, Response response) {
        view.showLoadingIndicator();
        view.navigateToPersonListActivity();
    }

    @Override
    public void onNetworkRequestFailed(LoginResponse userSession, Response response) {
        view.showLoadingIndicator();
        view.showLoginError(response.code() + "\n" + userSession.getError());
    }

    @Override
    public void onNetworkFailure(Throwable throwable) {

        view.showError(throwable);
    }

    @Override
    public void attachView(LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void subscribe() {
        loadCompositeDisposable();
    }

    @Override
    public void handleResponse(Response<LoginResponse> response) {

        Timber.d("Code: " + response.code() + " Message: " + response.message());
        LoginResponse body = response.body();

        if (body == null) {
            handleBodyErrorResponse(response);
        } else {
            handleBodyResponse(response);
            List<UserSession> data = body.getData();
            for (UserSession user : data) {
                BaseInteractor.API_TOKEN=user.getApiToken();
                loginRepository.saveUserSession(user,body.getAdditionalData());
            }

        }
    }

    @Override
    public void handleBodyResponse(Response<LoginResponse> response) {
        Timber.d(response.body().toString());
        onNetworkRequestSuccess(response.body(), response);

    }

    @Override
    public void handleBodyErrorResponse(Response<LoginResponse> responseError) {
        Gson gson = new Gson();
        LoginResponse userSessionError = gson.fromJson(responseError.errorBody().charStream(), LoginResponse.class);
        onNetworkRequestFailed(userSessionError, responseError);
    }

    //something really bad happened...
    @Override
    public void handleError(Throwable throwable) {
        //TODO:change Logger
        //Logger.d(throwable.getLocalizedMessage());
        onNetworkFailure(throwable);
    }


    @Override
    public void unsubscribe() {
        if (compositeDisposable != null) {
            if (!compositeDisposable.isDisposed()) {
                compositeDisposable.clear();
            }
        }
    }

    @Override
    public void destroy() {
        dispose();
    }


    @Override
    public void authenticateUser(String user, String password, boolean rememberUser) {
        view.showLoadingIndicator();
        compositeDisposable.add(loginRepository.getUserSession(user, password, rememberUser)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .cache()
                .subscribe(new Consumer<Response<LoginResponse>>() {
                    @Override
                    public void accept(Response<LoginResponse> userResponse) throws Exception {
                        handleResponse(userResponse);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //Handle a case for a custom Exception
                        if (throwable instanceof LoginRemoteDataSource.RemoteCacheNotEmptyException) {
                            handleError(throwable);
                        } else if (throwable instanceof NullPointerException) {

                        } else {
                            handleError(throwable);
                        }
                    }
                }));
    }

    @Override
    public void result(int requestCode, int resultCode) {
    }

    @Override
    public void doForgetSession() {
        loginRepository.deleteUserSession("");
    }

    @Override
    public void doRememberSession(boolean rememberUser) {
        loginRepository.rememberUserSession(rememberUser);
    }

    @Override
    public void getCachedUserSession() {
        view.showLoadingIndicator();
        loadCompositeDisposable();
        compositeDisposable.add(loginRepository.getCacheUserSession().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Consumer<Response<LoginResponse>>() {
                    @Override
                    public void accept(Response<LoginResponse> userResponse) throws Exception {
                        onLoadCachedUserSession(userResponse);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        handleError(throwable);
                    }
                }));
    }

    @Override
    public void onLoadCachedUserSession(Response<LoginResponse> response) {
        LoginResponse body = response.body();
        AdditionalData additionalData=body.getAdditionalData();
        List<UserSession> sessions = body.getData();
        for (UserSession session:sessions) {
            if(session.getApiToken()!=null ||!session.getApiToken().isEmpty()){
                view.onLoadCacheUserSession(session,additionalData);
                return;
            }

        }

    }

    private void loadCompositeDisposable() {
        if (compositeDisposable != null) {
            if (compositeDisposable.isDisposed()) {
                compositeDisposable = new CompositeDisposable();
            }
            return;
        }
        compositeDisposable = new CompositeDisposable();
    }

    private void dispose() {
        compositeDisposable.dispose();
    }
}
