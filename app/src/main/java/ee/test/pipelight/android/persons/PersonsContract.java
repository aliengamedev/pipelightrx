package ee.test.pipelight.android.persons;

import ee.test.pipelight.android.commons.base.BasePresenter;
import ee.test.pipelight.android.commons.base.BaseView;
import ee.test.pipelight.android.persons.model.remote.PersonsResponse;
import retrofit2.Response;


/**
 * Created by John on 10/22/2017.
 */

public interface PersonsContract {
    interface View extends BaseView<PersonsPresenter> {
        //Presenter -> View
        void showLoadingIndicator();
        void showError(Throwable throwable);
        void showTokenError(String msgError);
        void navigateToPersonDetailsActivity();
        void onLoadedCachePersons(PersonsResponse cachedPersons);
        void onLoadedPersons(PersonsResponse persons);
    }

    interface Presenter extends BasePresenter<View> {
        void result(int requestCode, int resultCode);
        //View -> Presenter
        void getCachedPersons();
        void getPersons();
        //Presenter -> View
        void onLoadCachedPersons(PersonsResponse loadedPersons);
    }

    interface Model<T> {
        // Presenter ->View
        void onNetworkRequestSuccess(PersonsResponse persons, Response response);
        void onNetworkRequestFailed(PersonsResponse persons, Response response);
        void onNetworkFailure(Throwable throwable);
        //Model -> Presenter
        void handleResponse(Response<T> response);
        void handleBodyResponse(Response<T> response);
        void handleBodyErrorResponse(Response<T> responseError);
        void handleError(Throwable throwable);
        //Presenter -> Model


    }
}
