package ee.test.pipelight.android.login.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/18/2017.
 */

public class AdditionalData {
    @SerializedName("user")
    @Expose
    public User user;
    @SerializedName("multiple_companies")
    @Expose
    public Boolean multipleCompanies;
    @SerializedName("default_company_id")
    @Expose
    public Integer defaultCompanyId;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getMultipleCompanies() {
        return multipleCompanies;
    }

    public void setMultipleCompanies(Boolean multipleCompanies) {
        this.multipleCompanies = multipleCompanies;
    }

    public Integer getDefaultCompanyId() {
        return defaultCompanyId;
    }

    public void setDefaultCompanyId(Integer defaultCompanyId) {
        this.defaultCompanyId = defaultCompanyId;
    }

    @Override
    public String toString() {
        return "AdditionalData{" +
                "user=" + user +
                ", multipleCompanies=" + multipleCompanies +
                ", defaultCompanyId=" + defaultCompanyId +
                '}';
    }
}
