package ee.test.pipelight.android.commons;

import timber.log.Timber;

/**
 * Created by John on 10/22/2017.
 */

public class PipeLogger extends Timber.DebugTree {

    @Override
    protected String createStackElementTag(StackTraceElement element) {
        return String.format("[L:%s] [M:%s] [C:%s]",
                element.getLineNumber(),
                element.getMethodName(),
                super.createStackElementTag(element));
    }
}