package ee.test.pipelight.android.persons.model;

import android.support.annotation.NonNull;

import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.persons.model.remote.PersonsResponse;
import ee.test.pipelight.android.persons.model.remote.dto.Datum;
import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by John on 10/22/2017.
 */

public interface PersonsDataSource {

     Observable<Response<PersonsResponse>> getPersons();

     void savePersons(@NonNull Datum userSession, @NonNull AdditionalData additionalData);
}
