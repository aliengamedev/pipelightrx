package ee.test.pipelight.android.login.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/19/2017.
 */

public class User {
    @SerializedName("profile")
    @Expose
    private Profile profile;
    @SerializedName("locale")
    @Expose
    private Locale locale;
    @SerializedName("timezone")
    @Expose
    private Timezone timezone;

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Timezone getTimezone() {
        return timezone;
    }

    public void setTimezone(Timezone timezone) {
        this.timezone = timezone;
    }

    @Override
    public String toString() {
        return "User{" +
                "profile=" + profile +
                ", locale=" + locale +
                ", timezone=" + timezone +
                '}';
    }
}
