package ee.test.pipelight.android.login.model.remote;

import java.util.List;

import ee.test.pipelight.android.commons.base.BaseResponse;
import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.login.model.remote.dto.UserSession;

/**
 * Created by John on 10/18/2017.
 */

public class LoginResponse extends BaseResponse<List<UserSession>,AdditionalData> {

    public LoginResponse(List<UserSession> userSessions,AdditionalData additionalData) {
        super(userSessions,additionalData);
    }
}
