package ee.test.pipelight.android.login.model.remote.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/18/2017.
 */

public class Info {
    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("creator_company_id")
    private Object creatorCompanyId;
    @SerializedName("plan_id")
    private Integer planId;
    @SerializedName("identifier")
    private String identifier;
    @SerializedName("domain")
    private String domain;
    @SerializedName("billing_currency")
    private String billingCurrency;
    @SerializedName("add_time")
    private String addTime;
    @SerializedName("status")
    private String status;
    @SerializedName("trial_ends")
    private String trialEnds;
    @SerializedName("cancelled_flag")
    private Boolean cancelledFlag;
    @SerializedName("cancel_time")
    private Object cancelTime;
    @SerializedName("country")
    private String country;
    @SerializedName("promo_code")
    private String promoCode;
    @SerializedName("used_promo_code_key")
    private Object usedPromoCodeKey;
    @SerializedName("account_is_open")
    private Boolean accountIsOpen;
    @SerializedName("account_is_not_paying")
    private Boolean accountIsNotPaying;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getCreatorCompanyId() {
        return creatorCompanyId;
    }

    public void setCreatorCompanyId(Object creatorCompanyId) {
        this.creatorCompanyId = creatorCompanyId;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }
}
