package ee.test.pipelight.android.commons;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

/**
 * Created by John on 10/21/2017.
 */

/**
 * With this implementation can create File logs to keep track of the important aspect of the app.
 */
public class FileLoggingTree extends Timber.Tree {
    private static final String TAG = FileLoggingTree.class.getSimpleName();
    private Context context;

    public FileLoggingTree(Context context) {
        this.context = context;
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {

        try {
            String pathname = context.getApplicationContext().getExternalCacheDir() + "/pipelightLogs";
            Log.d(TAG,"--> "+pathname);
            File direct = new File(pathname);
            if (!direct.exists()) {
                direct.mkdir();
            }
            String fileNameTimeStamp = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            String logTimeStamp = new SimpleDateFormat("E MMM dd yyyy 'at' hh:mm:ss:SSS aaa",
                    Locale.getDefault()).format(new Date());

            String fileName = fileNameTimeStamp + ".html";

            File file = new File(pathname
                    + File.separator
                    + fileName);

            file.createNewFile();
            if (file.exists()) {
                OutputStream fileOutputStream = new FileOutputStream(file, true);
                fileOutputStream.write(("<p style=\"background:lightgray;\"><strong style=\"background:lightblue;\">&nbsp&nbsp"
                        + logTimeStamp
                        + " :&nbsp&nbsp</strong>&nbsp&nbsp"
                        + message + "</p>").getBytes());
                fileOutputStream.close();
            }

        } catch (Exception e) {
            Log.e(TAG, "Error while logging into file : " + e);
        }
    }
}