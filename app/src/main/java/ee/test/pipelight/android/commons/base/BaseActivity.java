package ee.test.pipelight.android.commons.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.facebook.stetho.Stetho;

import butterknife.ButterKnife;
import ee.test.pipelight.BuildConfig;
import ee.test.pipelight.R;
import ee.test.pipelight.android.login.model.remote.dto.UserSession;


/**
 * Created by John on 10/17/2017.
 */
public class BaseActivity extends AppCompatActivity  {
    public ProgressDialog progressDialog;
    private BasePresenter presenter;
    @javax.annotation.Nullable
    UserSession userSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.DEBUG) {

        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    @Override
    protected void onStart() {
        presenter.subscribe();

        super.onStart();
    }

    @Override
    protected void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        presenter.unsubscribe();
        super.onStop();
    }

    @Override
    protected void onResume() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        presenter.subscribe();
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        presenter.subscribe();
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void setupToolbar(final String title) {
        setupToolbar(title, true);
    }

    protected Toolbar getToolbar() {
        return (Toolbar) findViewById(R.id.main_toolbar);
    }

    protected void setupToolbar(final String title, final boolean enableUpNavigation) {
        final Toolbar toolbar = getToolbar();
        if (toolbar == null) {
            return;
        }
        toolbar.setTitleTextAppearance(this, R.style.CodeFont);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        if (enableUpNavigation) {
            toolbar.setNavigationIcon(R.drawable.ic_action_name);
        }
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this);
    }

    public void messageDialog(String tile, String msgError, int drawableId) {
        isProgressDialogInit();
        progressDialog = ProgressDialog.show(this, tile, msgError, false, true);
        progressDialog.setMax(10);
        progressDialog.setIndeterminateDrawable(getResources().getDrawable(drawableId, null));
        progressDialog.setIndeterminate(false);
    }

    private void isProgressDialogInit() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void messageDialog(String title, String message, boolean indeterminate, boolean cancelable, int drawableId) {
        isProgressDialogInit();
        progressDialog = ProgressDialog.show(this, title, message, indeterminate, cancelable, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
        progressDialog.setMax(10);
        progressDialog.setIndeterminateDrawable(getResources().getDrawable(drawableId, null));
        progressDialog.setIndeterminate(false);
    }


    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected void setPresenter(BasePresenter presenter) {
        this.presenter = presenter;
    }

}