package ee.test.pipelight.android.persons.ui;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.stetho.json.ObjectMapper;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import ee.test.pipelight.R;
import ee.test.pipelight.android.persons.model.remote.dto.Datum;
import ee.test.pipelight.android.persons.model.remote.dto.Person;
import ee.test.pipelight.android.persons.model.remote.dto.Phone;
import ee.test.pipelight.android.persons.model.remote.dto.PictureId_;
import timber.log.Timber;

/**
 * Created by John on 10/23/2017.
 */

public class PersonListAdapter extends RecyclerView.Adapter<PersonListAdapter.PersonViewHolder> {

    private final Context context;
    private LayoutInflater inflater;
    List<Datum> datums;

    public PersonListAdapter(Context context, List<Datum> datums) {
        this.context = context;
        this.datums = datums;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.include_person_card, parent, false);
        return new PersonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        holder.name.setText(datums.get(position).name);
        List<Phone> phoneData = datums.get(position).phone;
        if (!phoneData.isEmpty()) {
            holder.phone.setText(phoneData.get(0).value);
        }

        PictureId_ pictureId_ = datums.get(position).pictureId;
        if (pictureId_ != null) {
            if (pictureId_.pictures != null) {
                Timber.d("PersonalListAdapater: " + pictureId_.pictures);
                ObjectMapper m = new ObjectMapper();
                Map<String, String> pictureMap = m.convertValue(pictureId_.pictures, Map.class);

                for (Map.Entry<String, String> entry : pictureMap.entrySet()) {
                    String pictureUrl = entry.getValue();
                    Timber.d("Key : " + entry.getKey() + " Value : " + pictureUrl);
                    if (!pictureUrl.isEmpty()) {
                        Picasso.with(context).load(pictureUrl).into(holder.opPhoto);
                        return;
                    }
                }

                // holder.opPhoto.setImageDrawable(context.getDrawable(R.drawable.ic_action_name));
            } else {
                //
                holder.opPhoto.setImageDrawable(context.getDrawable(R.drawable.ic_action_name));
            }
        } else {
            holder.opPhoto.setImageDrawable(context.getDrawable(R.drawable.ic_action_name));
        }
    }

    @Override
    public int getItemCount() {
        return datums.size();
    }

    public void updateAdapter(List<Datum> datums) {
        this.datums.clear();
        this.datums.addAll(datums);
        notifyDataSetChanged();
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.person_card_view)
        CardView cv;
        @BindView(R.id.person_name_val)
        TextView name;
        @BindView(R.id.person_phone_val)
        TextView phone;
        @BindView(R.id.person_photo)
        ImageView opPhoto;


        public PersonViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
