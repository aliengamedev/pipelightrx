package ee.test.pipelight.android.login.model.local.db;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import ee.test.pipelight.android.commons.AppDatabase;

/**
 * Created by John on 10/22/2017.
 */
@Table(database = AppDatabase.class, name = "tblUserSession3")
public class UsserSession extends BaseModel{

    @Column
    @PrimaryKey(autoincrement = true)
    int id;
    /**
     * Yeahh emmm you won't belive what just happened.
     */
    @Column(name="userId")
    String json;

    @Column
    String apiToken;

     @Column(name="timestamp")
     String timestamp;

    @Column(name="addtionalData")
    String additionalData;
    boolean checked = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }
}
