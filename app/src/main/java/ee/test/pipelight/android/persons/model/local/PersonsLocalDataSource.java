package ee.test.pipelight.android.persons.model.local;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.persons.PersonsContract;
import ee.test.pipelight.android.persons.model.PersonsDataSource;
import ee.test.pipelight.android.persons.model.remote.PersonsResponse;
import ee.test.pipelight.android.persons.model.remote.dto.Datum;
import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by John on 10/23/2017.
 */

public class PersonsLocalDataSource implements PersonsDataSource {


    @Nullable
    private static PersonsLocalDataSource INSTANCE;

    private boolean persistSession;

    private PersonsContract.Model listener;

    private PersonsLocalDataSource(@NonNull PersonsContract.Model listener) {
        this.listener = listener;
    }


    public static PersonsLocalDataSource getInstance(@NonNull PersonsContract.Model listener) {
        if (INSTANCE == null) {
            INSTANCE = new PersonsLocalDataSource(listener);
        }
        return INSTANCE;
    }

    @Override
    public Observable<Response<PersonsResponse>> getPersons() {
        Observable<Response<PersonsResponse>> obEmptyResponse = Observable.empty();
        return obEmptyResponse;
    }

    @Override
    public void savePersons(@NonNull Datum userSession, @NonNull AdditionalData additionalData) {

    }
}
