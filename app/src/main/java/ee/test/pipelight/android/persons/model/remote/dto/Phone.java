package ee.test.pipelight.android.persons.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/23/2017.
 */

public class Phone {
    @SerializedName("label")
    @Expose
    public String label;
    @SerializedName("value")
    @Expose
    public String value;
    @SerializedName("primary")
    @Expose
    public Boolean primary;
}
