package ee.test.pipelight.android.Utils;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;
import com.raizlabs.android.dbflow.sql.migration.BaseMigration;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;

import java.io.InputStream;

import ee.test.pipelight.android.commons.AppDatabase;
import ee.test.pipelight.android.login.model.local.db.UsserSession;
import timber.log.Timber;

/**
 * Created by John on 10/20/2017.
 */

public class Utils {
    /**
     * Utility to parse small files as a string
     * @param context
     * @param file
     * @return
     */
    public static String readFromFile(Context context, String file) {
        try {
            InputStream is = context.getAssets().open(file);
            int size = is.available();
            byte buffer[] = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (Exception e) {
            e.printStackTrace();
            return "" ;
        }
    }

    public static MockAccount getMockAccount(Context context){
       return  new Gson().fromJson(readFromFile(context,"acc.dat"), MockAccount.class);
    }

}
