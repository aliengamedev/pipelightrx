package ee.test.pipelight.android.persons.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.persons.PersonsContract;
import ee.test.pipelight.android.persons.model.remote.PersonsResponse;
import ee.test.pipelight.android.persons.model.remote.dto.Datum;
import io.reactivex.Observable;
import io.reactivex.functions.Predicate;
import retrofit2.Response;

/**
 * Created by John on 10/22/2017.
 */

public class PersonRepository implements PersonsDataSource {


    @NonNull
    private final PersonsDataSource remoteDataSource;

    @NonNull
    private final PersonsDataSource localDataSource;

    @Nullable
    private static PersonRepository INSTANCE;

    private boolean persistSession;

    private PersonRepository(@NonNull PersonsContract.Model listener, @NonNull PersonsDataSource remoteDataSource, @NonNull PersonsDataSource localDataSource) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
    }


    public static PersonRepository getInstance(@NonNull PersonsContract.Model listener, @NonNull PersonsDataSource remoteDataSource, @NonNull PersonsDataSource localDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new PersonRepository(listener, remoteDataSource, localDataSource);
        }
        return INSTANCE;
    }

    @Override
    public Observable<Response<PersonsResponse>> getPersons() {
        return remoteDataSource.getPersons();
    }

    @Override
    public void savePersons(@NonNull Datum userSession, @NonNull AdditionalData additionalData) {

    }
}
