package ee.test.pipelight.android.persons.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by John on 10/23/2017.
 */

public class Datum {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("company_id")
    @Expose
    public Integer companyId;
    @SerializedName("owner_id")
    @Expose
    public OwnerId ownerId;
    @SerializedName("org_id")
    @Expose
    public OrgId orgId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("open_deals_count")
    @Expose
    public Integer openDealsCount;
    @SerializedName("related_open_deals_count")
    @Expose
    public Integer relatedOpenDealsCount;
    @SerializedName("closed_deals_count")
    @Expose
    public Integer closedDealsCount;
    @SerializedName("related_closed_deals_count")
    @Expose
    public Integer relatedClosedDealsCount;
    @SerializedName("participant_open_deals_count")
    @Expose
    public Integer participantOpenDealsCount;
    @SerializedName("participant_closed_deals_count")
    @Expose
    public Integer participantClosedDealsCount;
    @SerializedName("email_messages_count")
    @Expose
    public Integer emailMessagesCount;
    @SerializedName("activities_count")
    @Expose
    public Integer activitiesCount;
    @SerializedName("done_activities_count")
    @Expose
    public Integer doneActivitiesCount;
    @SerializedName("undone_activities_count")
    @Expose
    public Integer undoneActivitiesCount;
    @SerializedName("reference_activities_count")
    @Expose
    public Integer referenceActivitiesCount;
    @SerializedName("files_count")
    @Expose
    public Integer filesCount;
    @SerializedName("notes_count")
    @Expose
    public Integer notesCount;
    @SerializedName("followers_count")
    @Expose
    public Integer followersCount;
    @SerializedName("won_deals_count")
    @Expose
    public Integer wonDealsCount;
    @SerializedName("related_won_deals_count")
    @Expose
    public Integer relatedWonDealsCount;
    @SerializedName("lost_deals_count")
    @Expose
    public Integer lostDealsCount;
    @SerializedName("related_lost_deals_count")
    @Expose
    public Integer relatedLostDealsCount;
    @SerializedName("active_flag")
    @Expose
    public Boolean activeFlag;
    @SerializedName("phone")
    @Expose
    public List<Phone> phone = null;
    @SerializedName("email")
    @Expose
    public List<Email> email = null;
    @SerializedName("first_char")
    @Expose
    public String firstChar;
    @SerializedName("update_time")
    @Expose
    public String updateTime;
    @SerializedName("add_time")
    @Expose
    public String addTime;
    @SerializedName("visible_to")
    @Expose
    public String visibleTo;
    @SerializedName("picture_id")
    @Expose
    public PictureId_ pictureId;
    @SerializedName("next_activity_date")
    @Expose
    public Object nextActivityDate;
    @SerializedName("next_activity_time")
    @Expose
    public Object nextActivityTime;
    @SerializedName("next_activity_id")
    @Expose
    public Object nextActivityId;
    @SerializedName("last_activity_id")
    @Expose
    public Object lastActivityId;
    @SerializedName("last_activity_date")
    @Expose
    public Object lastActivityDate;
    @SerializedName("timeline_last_activity_time")
    @Expose
    public Object timelineLastActivityTime;
    @SerializedName("timeline_last_activity_time_by_owner")
    @Expose
    public Object timelineLastActivityTimeByOwner;
    @SerializedName("last_incoming_mail_time")
    @Expose
    public Object lastIncomingMailTime;
    @SerializedName("last_outgoing_mail_time")
    @Expose
    public Object lastOutgoingMailTime;
    @SerializedName("org_name")
    @Expose
    public String orgName;
    @SerializedName("owner_name")
    @Expose
    public String ownerName;
    @SerializedName("cc_email")
    @Expose
    public String ccEmail;


    @Override
    public String toString() {
        return "Datum{" +
                "id=" + id +
                ", companyId=" + companyId +
                ", ownerId=" + ownerId +
                ", orgId=" + orgId +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", openDealsCount=" + openDealsCount +
                ", relatedOpenDealsCount=" + relatedOpenDealsCount +
                ", closedDealsCount=" + closedDealsCount +
                ", relatedClosedDealsCount=" + relatedClosedDealsCount +
                ", participantOpenDealsCount=" + participantOpenDealsCount +
                ", participantClosedDealsCount=" + participantClosedDealsCount +
                ", emailMessagesCount=" + emailMessagesCount +
                ", activitiesCount=" + activitiesCount +
                ", doneActivitiesCount=" + doneActivitiesCount +
                ", undoneActivitiesCount=" + undoneActivitiesCount +
                ", referenceActivitiesCount=" + referenceActivitiesCount +
                ", filesCount=" + filesCount +
                ", notesCount=" + notesCount +
                ", followersCount=" + followersCount +
                ", wonDealsCount=" + wonDealsCount +
                ", relatedWonDealsCount=" + relatedWonDealsCount +
                ", lostDealsCount=" + lostDealsCount +
                ", relatedLostDealsCount=" + relatedLostDealsCount +
                ", activeFlag=" + activeFlag +
                ", phone=" + phone +
                ", email=" + email +
                ", firstChar='" + firstChar + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", addTime='" + addTime + '\'' +
                ", visibleTo='" + visibleTo + '\'' +
                ", pictureId=" + pictureId +
                ", nextActivityDate=" + nextActivityDate +
                ", nextActivityTime=" + nextActivityTime +
                ", nextActivityId=" + nextActivityId +
                ", lastActivityId=" + lastActivityId +
                ", lastActivityDate=" + lastActivityDate +
                ", timelineLastActivityTime=" + timelineLastActivityTime +
                ", timelineLastActivityTimeByOwner=" + timelineLastActivityTimeByOwner +
                ", lastIncomingMailTime=" + lastIncomingMailTime +
                ", lastOutgoingMailTime=" + lastOutgoingMailTime +
                ", orgName='" + orgName + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", ccEmail='" + ccEmail + '\'' +
                '}';
    }
}
