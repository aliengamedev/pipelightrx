package ee.test.pipelight.android.login.model.remote;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import ee.test.pipelight.android.commons.base.BaseInteractor;
import ee.test.pipelight.android.login.LoginContract;
import ee.test.pipelight.android.login.model.LoginDataSource;
import ee.test.pipelight.android.login.model.remote.dto.AdditionalData;
import ee.test.pipelight.android.login.model.remote.dto.UserSession;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import retrofit2.Response;
import retrofit2.Retrofit;
import timber.log.Timber;

/**
 * Created by John on 10/20/2017.
 */

public class LoginRemoteDataSource extends BaseInteractor<LoginResponse> implements LoginDataSource {

    @Nullable
    private static LoginRemoteDataSource INSTANCE;
    //Todo:Probably i don't need this here Login Data source should remain separated
    private LoginContract.Model listener;
    private final static Map<String, UserSession> LOGIN_USERSESSIONS_DATA;
    private final static Map<String, AdditionalData> LOGIN_ADDTIONAL_DATA;
    private boolean persistSession;

    static {
        LOGIN_USERSESSIONS_DATA = new LinkedHashMap<>(2);
        LOGIN_ADDTIONAL_DATA = new LinkedHashMap<>(2);
    }

    private LoginRemoteDataSource(@NonNull LoginContract.Model listener) {
        this.listener = listener;

    }

    public static LoginRemoteDataSource getInstance(@NonNull LoginContract.Model listener) {
        if (INSTANCE == null) {
            INSTANCE = new LoginRemoteDataSource(listener);
        }
        return INSTANCE;
    }

    @Override
    public Observable<Response<LoginResponse>> getUserSession(String username, String password,boolean rememberUser) {
        Retrofit retrofit = initRetrofit();
        PipeDriveService pipeDriveService = retrofit.create(PipeDriveService.class);
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail(username);
        loginRequest.setPassword(password);
        rememberUserSession(rememberUser);
        if (LOGIN_USERSESSIONS_DATA.size() == 0) {
            //TODO:Cache is clean -> check DB ->[sessionFound -> returns UserSession ,sessionNotFound->promnts to Login]
            return pipeDriveService.login(loginRequest);
        } else {
            Observable<Response<LoginResponse>> emptyResponse = getResponseObservable();
            return emptyResponse;
        }
    }

    private Observable<Response<LoginResponse>> getResponseObservable() {
        final List<UserSession> userSessions = new ArrayList<>();
        Observable.fromIterable(LOGIN_USERSESSIONS_DATA.values()).subscribe(new Consumer<UserSession>() {
            @Override
            public void accept(@io.reactivex.annotations.NonNull UserSession userSession) throws Exception {
                Timber.d("ElementsInCahce:" + userSession.getCompanyId() + " " + userSession.getAddTime());
                userSessions.add(userSession);
            }
        }).dispose();
         AdditionalData additionalData;
        final List<AdditionalData> additionalDataCont = new ArrayList<>();
        Observable.fromIterable(LOGIN_ADDTIONAL_DATA.values()).subscribe(new Consumer<AdditionalData>() {
            @Override
            public void accept(@io.reactivex.annotations.NonNull AdditionalData _additionalData) throws Exception {
                Timber.d("AddtionalData ElementsInCahce:" + _additionalData.getUser().getProfile().getEmail() );
                additionalDataCont.add(_additionalData);
            }
        }).dispose();
        Timber.d("ElementsInCahce: Done iterating them send cached response");
        AdditionalData additionalData1 = additionalDataCont.get(0);
        Response<LoginResponse> loginResponseResponse = Response.success(new LoginResponse(userSessions, additionalData1));
        return Observable.just(loginResponseResponse);
    }

    @Override
    public Observable<List<UserSession>> getUserSession() {
        //Observable<List<UserSession>> emptyResponse = Observable.error(new RemoteCacheNotEmptyException("cache.size="+LOGIN_USERSESSIONS_DATA.size()));
        List<UserSession> items = new ArrayList<>();
        //Empty list response example
        Observable<List<UserSession>> emptyResponse = Observable.just(items);
        return emptyResponse;
    }

    @Override
    public Observable<Response<LoginResponse>> getCacheUserSession() {
        //Observable<List<UserSession>> emptyResponse = Observable.error(new RemoteCacheNotEmptyException("cache.size="+LOGIN_USERSESSIONS_DATA.size()));
        List<UserSession> items = new ArrayList<>();
        //Empty list response example
        Observable<Response<LoginResponse>> emptyResponse = Observable.empty();
        return emptyResponse;
    }

    @Override
    public void saveUserSession(@NonNull UserSession userSession,@NonNull AdditionalData additionalData) {
        if (!LOGIN_USERSESSIONS_DATA.containsKey(userSession.getAddTime())) {
            LOGIN_USERSESSIONS_DATA.put(userSession.getAddTime(), userSession);
        }else{
            Timber.e("UserSession is already in cached");
        }
        if (!LOGIN_ADDTIONAL_DATA.containsKey(userSession.getAddTime())) {
            LOGIN_ADDTIONAL_DATA.put(userSession.getAddTime(), additionalData);
        }else{
            Timber.e("UserSession is already in cached");
        }
    }

    @Override
    public void deleteUserSession(@NonNull String usrSessionId) {
        LOGIN_USERSESSIONS_DATA.clear();
    }

    @Override
    public void rememberUserSession(boolean remember) {
        this.persistSession=remember;
    }

    /**
     * Custom Exception
     */
    public class RemoteCacheNotEmptyException extends Exception {
        // Parameterless Constructor
        public RemoteCacheNotEmptyException() {
        }

        // Constructor that accepts a message
        public RemoteCacheNotEmptyException(String message) {
            super(message);
        }
    }

}
