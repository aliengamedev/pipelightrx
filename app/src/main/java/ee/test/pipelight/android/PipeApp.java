package ee.test.pipelight.android;

import android.app.Application;
import android.content.res.Configuration;

import com.facebook.stetho.Stetho;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowLog;
import com.raizlabs.android.dbflow.config.FlowManager;

import ee.test.pipelight.android.Utils.Utils;
import ee.test.pipelight.android.commons.AppDatabase;
import ee.test.pipelight.android.commons.PipeLogger;
import ee.test.pipelight.android.login.model.local.db.UsserSession;
import timber.log.Timber;

/**
 * Created by John on 10/22/2017.
 */

public class PipeApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Required initialization logic here!
        FlowManager.init(new FlowConfig.Builder(getApplicationContext())
                .openDatabasesOnInit(true)
                .build());
        FlowLog.setMinimumLoggingLevel(FlowLog.Level.V);
        Timber.plant(new PipeLogger());
        Stetho.initializeWithDefaults(this);
       // dbMigration();

    }

    public void  dbMigration(){
        AppDatabase.Migration7 migrateAccountsTo7 =new AppDatabase.Migration7(UsserSession.class);
        migrateAccountsTo7.onPreMigrate();
    }

    // Called by the system when the device configuration changes while your component is running.
    // Overriding this method is totally optional!
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // This is called when the overall system is running low on memory,
    // and would like actively running processes to tighten their belts.
    // Overriding this method is totally optional!
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
