package ee.test.pipelight.android.commons.base;
/**
 * Created by John on 10/16/2017.
 */
public interface BasePresenter<T> {

    void attachView(T view);
    void detachView();
    void subscribe();
    void unsubscribe();
    void destroy();
}
