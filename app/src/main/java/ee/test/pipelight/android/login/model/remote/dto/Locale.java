package ee.test.pipelight.android.login.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/19/2017.
 */

public class Locale {
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("uses_12_hour_clock")
    @Expose
    private Boolean uses12HourClock;


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Boolean getUses12HourClock() {
        return uses12HourClock;
    }

    public void setUses12HourClock(Boolean uses12HourClock) {
        this.uses12HourClock = uses12HourClock;
    }

    @Override
    public String toString() {
        return "Locale{" +
                "language='" + language + '\'' +
                ", country='" + country + '\'' +
                ", uses12HourClock=" + uses12HourClock +
                '}';
    }
}
