package ee.test.pipelight.android.persons.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/23/2017.
 */
public class Email {

    @SerializedName("label")
    @Expose
    public String label;
    @SerializedName("value")
    @Expose
    public String value;
    @SerializedName("primary")
    @Expose
    public Boolean primary;

    @Override
    public String toString() {
        return "Email{" +
                "label='" + label + '\'' +
                ", value='" + value + '\'' +
                ", primary=" + primary +
                '}';
    }
}