package ee.test.pipelight.android.login.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/19/2017.
 */

public class Timezone {
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("offset")
    @Expose
    public Integer offset;

}
