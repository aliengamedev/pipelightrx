package ee.test.pipelight.android.persons.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/23/2017.
 */

public class OrgId {
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("people_count")
    @Expose
    public Integer peopleCount;
    @SerializedName("owner_id")
    @Expose
    public Integer ownerId;
    @SerializedName("address")
    @Expose
    public Object address;
    @SerializedName("cc_email")
    @Expose
    public String ccEmail;
    @SerializedName("value")
    @Expose
    public Integer value;

    @Override
    public String toString() {
        return "OrgId{" +
                "name='" + name + '\'' +
                ", peopleCount=" + peopleCount +
                ", ownerId=" + ownerId +
                ", address=" + address +
                ", ccEmail='" + ccEmail + '\'' +
                ", value=" + value +
                '}';
    }
}
