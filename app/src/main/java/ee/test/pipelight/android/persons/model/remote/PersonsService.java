package ee.test.pipelight.android.persons.model.remote;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by John on 10/23/2017.
 */

public interface PersonsService {

    @GET("persons")
    Observable<Response<PersonsResponse>> persons(@Query("api_token") String apiToken);
}
