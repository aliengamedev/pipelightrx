package ee.test.pipelight.android.persons.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by John on 10/23/2017.
 */

public class OwnerId {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("has_pic")
    @Expose
    public Boolean hasPic;
    @SerializedName("pic_hash")
    @Expose
    public String picHash;
    @SerializedName("active_flag")
    @Expose
    public Boolean activeFlag;
    @SerializedName("value")
    @Expose
    public Integer value;

    @Override
    public String toString() {
        return "OwnerId{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", hasPic=" + hasPic +
                ", picHash='" + picHash + '\'' +
                ", activeFlag=" + activeFlag +
                ", value=" + value +
                '}';
    }
}
