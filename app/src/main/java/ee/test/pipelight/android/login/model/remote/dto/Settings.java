package ee.test.pipelight.android.login.model.remote.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by John on 10/18/2017.
 */

public class Settings {

    @SerializedName("show_getting_started_video")
    @Expose
    private Boolean showGettingStartedVideo;
    @SerializedName("list_limit")
    @Expose
    private Integer listLimit;
    @SerializedName("beta_app")
    @Expose
    private Boolean betaApp;
    @SerializedName("file_upload_destination")
    @Expose
    private String fileUploadDestination;
    @SerializedName("callto_link_syntax")
    @Expose
    private String calltoLinkSyntax;
    @SerializedName("autofill_deal_expected_close_date")
    @Expose
    private Boolean autofillDealExpectedCloseDate;
    @SerializedName("person_duplicate_condition")
    @Expose
    private String personDuplicateCondition;
    @SerializedName("organization_duplicate_condition")
    @Expose
    private String organizationDuplicateCondition;
    @SerializedName("add_followers_when_importing")
    @Expose
    private Boolean addFollowersWhenImporting;
    @SerializedName("search_backend")
    @Expose
    private String searchBackend;
    @SerializedName("billing_managed_by_sales")
    @Expose
    private Boolean billingManagedBySales;
    @SerializedName("max_deal_age_in_average_progress_calculation")
    @Expose
    private Integer maxDealAgeInAverageProgressCalculation;
    @SerializedName("third_party_links")
    @Expose
    private List<Object> thirdPartyLinks = null;
    @SerializedName("elastic_write_target_during_migration")
    @Expose
    private String elasticWriteTargetDuringMigration;
    @SerializedName("auto_create_new_persons_from_forwarder_emails")
    @Expose
    private Boolean autoCreateNewPersonsFromForwarderEmails;
    @SerializedName("company_advanced_debug_logs")
    @Expose
    private Boolean companyAdvancedDebugLogs;
    @SerializedName("deal_block_order")
    @Expose
    private List<DealBlockOrder> dealBlockOrder = null;
    @SerializedName("person_block_order")
    @Expose
    private List<PersonBlockOrder> personBlockOrder = null;
    @SerializedName("organization_block_order")
    @Expose
    private List<OrganizationBlockOrder> organizationBlockOrder = null;
    @SerializedName("nylas_sync")
    @Expose
    private Boolean nylasSync;
    @SerializedName("onboarding_complete")
    @Expose
    private Boolean onboardingComplete;

    public Boolean getShowGettingStartedVideo() {
        return showGettingStartedVideo;
    }

    public void setShowGettingStartedVideo(Boolean showGettingStartedVideo) {
        this.showGettingStartedVideo = showGettingStartedVideo;
    }

    public Integer getListLimit() {
        return listLimit;
    }

    public void setListLimit(Integer listLimit) {
        this.listLimit = listLimit;
    }

    public Boolean getBetaApp() {
        return betaApp;
    }

    public void setBetaApp(Boolean betaApp) {
        this.betaApp = betaApp;
    }

    public String getFileUploadDestination() {
        return fileUploadDestination;
    }

    public void setFileUploadDestination(String fileUploadDestination) {
        this.fileUploadDestination = fileUploadDestination;
    }

    public String getCalltoLinkSyntax() {
        return calltoLinkSyntax;
    }

    public void setCalltoLinkSyntax(String calltoLinkSyntax) {
        this.calltoLinkSyntax = calltoLinkSyntax;
    }

    public Boolean getAutofillDealExpectedCloseDate() {
        return autofillDealExpectedCloseDate;
    }

    public void setAutofillDealExpectedCloseDate(Boolean autofillDealExpectedCloseDate) {
        this.autofillDealExpectedCloseDate = autofillDealExpectedCloseDate;
    }

    public String getPersonDuplicateCondition() {
        return personDuplicateCondition;
    }

    public void setPersonDuplicateCondition(String personDuplicateCondition) {
        this.personDuplicateCondition = personDuplicateCondition;
    }

    public String getOrganizationDuplicateCondition() {
        return organizationDuplicateCondition;
    }

    public void setOrganizationDuplicateCondition(String organizationDuplicateCondition) {
        this.organizationDuplicateCondition = organizationDuplicateCondition;
    }

    public Boolean getAddFollowersWhenImporting() {
        return addFollowersWhenImporting;
    }

    public void setAddFollowersWhenImporting(Boolean addFollowersWhenImporting) {
        this.addFollowersWhenImporting = addFollowersWhenImporting;
    }

    public String getSearchBackend() {
        return searchBackend;
    }

    public void setSearchBackend(String searchBackend) {
        this.searchBackend = searchBackend;
    }

    public Boolean getBillingManagedBySales() {
        return billingManagedBySales;
    }

    public void setBillingManagedBySales(Boolean billingManagedBySales) {
        this.billingManagedBySales = billingManagedBySales;
    }

    public Integer getMaxDealAgeInAverageProgressCalculation() {
        return maxDealAgeInAverageProgressCalculation;
    }

    public void setMaxDealAgeInAverageProgressCalculation(Integer maxDealAgeInAverageProgressCalculation) {
        this.maxDealAgeInAverageProgressCalculation = maxDealAgeInAverageProgressCalculation;
    }

    public List<Object> getThirdPartyLinks() {
        return thirdPartyLinks;
    }

    public void setThirdPartyLinks(List<Object> thirdPartyLinks) {
        this.thirdPartyLinks = thirdPartyLinks;
    }

    public String getElasticWriteTargetDuringMigration() {
        return elasticWriteTargetDuringMigration;
    }

    public void setElasticWriteTargetDuringMigration(String elasticWriteTargetDuringMigration) {
        this.elasticWriteTargetDuringMigration = elasticWriteTargetDuringMigration;
    }

    public Boolean getAutoCreateNewPersonsFromForwarderEmails() {
        return autoCreateNewPersonsFromForwarderEmails;
    }

    public void setAutoCreateNewPersonsFromForwarderEmails(Boolean autoCreateNewPersonsFromForwarderEmails) {
        this.autoCreateNewPersonsFromForwarderEmails = autoCreateNewPersonsFromForwarderEmails;
    }

    public Boolean getCompanyAdvancedDebugLogs() {
        return companyAdvancedDebugLogs;
    }

    public void setCompanyAdvancedDebugLogs(Boolean companyAdvancedDebugLogs) {
        this.companyAdvancedDebugLogs = companyAdvancedDebugLogs;
    }

    public List<DealBlockOrder> getDealBlockOrder() {
        return dealBlockOrder;
    }

    public void setDealBlockOrder(List<DealBlockOrder> dealBlockOrder) {
        this.dealBlockOrder = dealBlockOrder;
    }

    public List<PersonBlockOrder> getPersonBlockOrder() {
        return personBlockOrder;
    }

    public void setPersonBlockOrder(List<PersonBlockOrder> personBlockOrder) {
        this.personBlockOrder = personBlockOrder;
    }

    public List<OrganizationBlockOrder> getOrganizationBlockOrder() {
        return organizationBlockOrder;
    }

    public void setOrganizationBlockOrder(List<OrganizationBlockOrder> organizationBlockOrder) {
        this.organizationBlockOrder = organizationBlockOrder;
    }

    public Boolean getNylasSync() {
        return nylasSync;
    }

    public void setNylasSync(Boolean nylasSync) {
        this.nylasSync = nylasSync;
    }

    public Boolean getOnboardingComplete() {
        return onboardingComplete;
    }

    public void setOnboardingComplete(Boolean onboardingComplete) {
        this.onboardingComplete = onboardingComplete;
    }

    @Override
    public String toString() {
        return "Settings{" +
                "showGettingStartedVideo=" + showGettingStartedVideo +
                ", listLimit=" + listLimit +
                ", betaApp=" + betaApp +
                ", fileUploadDestination='" + fileUploadDestination + '\'' +
                ", calltoLinkSyntax='" + calltoLinkSyntax + '\'' +
                ", autofillDealExpectedCloseDate=" + autofillDealExpectedCloseDate +
                ", personDuplicateCondition='" + personDuplicateCondition + '\'' +
                ", organizationDuplicateCondition='" + organizationDuplicateCondition + '\'' +
                ", addFollowersWhenImporting=" + addFollowersWhenImporting +
                ", searchBackend='" + searchBackend + '\'' +
                ", billingManagedBySales=" + billingManagedBySales +
                ", maxDealAgeInAverageProgressCalculation=" + maxDealAgeInAverageProgressCalculation +
                ", thirdPartyLinks=" + thirdPartyLinks +
                ", elasticWriteTargetDuringMigration='" + elasticWriteTargetDuringMigration + '\'' +
                ", autoCreateNewPersonsFromForwarderEmails=" + autoCreateNewPersonsFromForwarderEmails +
                ", companyAdvancedDebugLogs=" + companyAdvancedDebugLogs +
                ", dealBlockOrder=" + dealBlockOrder +
                ", personBlockOrder=" + personBlockOrder +
                ", organizationBlockOrder=" + organizationBlockOrder +
                ", nylasSync=" + nylasSync +
                ", onboardingComplete=" + onboardingComplete +
                '}';
    }
}
